import socket


nom=socket.gethostname()
ip=socket.gethostbyname(nom)
print("You are",nom,"at",ip)

while True:
        
        #-------- Receive a message
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((ip, 15555)) 
	s.listen(1)
	client, adress = s.accept()
	print( f"New message from {adress}:")
	response = client.recv(50)
	print(f"{response.decode()}")
	s.close()
	del s
	
	#-------- Send The reponse
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((adress[0],15555)) 
	s.send(input(">").encode())
	s.close()
